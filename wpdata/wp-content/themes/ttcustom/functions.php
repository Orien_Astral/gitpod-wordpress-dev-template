<?php

/* -- enqueu du style de parent -- */

function my_theme_enqueue_styles() {
    $parenthandle = 'twentytwenty-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
    $theme = wp_get_theme();
    wp_enqueue_style( $parenthandle, get_template_directory_uri() . '/style.css', 
        array(),  // if the parent theme code has a dependency, copy it to here
        $theme->parent()->get('Version')
    );
    wp_enqueue_style( 'child-style', get_stylesheet_uri(),
        array( $parenthandle ),
        $theme->get('Version') // this only works if you have Version in the style header
    );
}

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

/* -- Custom Post Type -- */

function concert_post_type() {
    /* Property */
	$labels = array(
		'name'                => _x('Concert', 'Post Type General Name'),
		'singular_name'       => _x('Concert', 'Post Type Singular Name'),
		'menu_name'           => __('Concert'),
		'name_admin_bar'      => __('Concert'),
		'parent_item_colon'   => __('Parent Item:'),
		'all_items'           => __('All Items'),
		'add_new_item'        => __('Add New Item'),
		'add_new'             => __('Add New'),
		'new_item'            => __('New Item' ),
		'edit_item'           => __('Edit Item'),
		'update_item'         => __('Update Item'),
		'view_item'           => __('View Item'),
		'search_items'        => __('Search Item'),
		'not_found'           => __('Not found'),
		'not_found_in_trash'  => __('Not found in Trash'),
	);
	$rewrite = array(
		'slug'                => _x('Concert', 'Concert'),
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => false,
	);
	$args = array(
		'label'               => __('Concert'),
		'description'         => __('Concert'),
		'labels'              => $labels,
		'supports'            => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'custom-fields'),
		'taxonomies'          => array('property_type'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-admin-home',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'query_var'           => 'Concert',
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type('Concert', $args);	
}

add_action('init', 'concert_post_type', 10);